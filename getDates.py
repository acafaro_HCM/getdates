import os
import datetime
import re

logs = os.listdir('./')
logdate = ""
now = datetime.datetime.now().date()

for log in logs:
	if re.search("20..\...\...", log): # only read file names with dates
		logdate = str(re.findall("20..\...\...", log))[2:-2].replace(".", "") # pull dates from log names
		logdate = datetime.datetime(int(logdate[:4]), int(logdate[5:6]), int(logdate[-2:])).date() # convert to date format
		day_diff = int(str(now - logdate).split(' ')[0]) # calculate time difference in days, remove tertiary info, convert to int

		if day_diff > 183: # if log is older than six months old, say so
			print(log, " is older than six months.")